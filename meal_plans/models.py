from django.db import models
from django.forms import DateTimeField
from django.conf import settings

USER_MODEL = settings.AUTH_USER_MODEL
# Create your models here.
class MealPlan(models.Model):
    name = models.CharField(max_length = 120)
    date = models.DateTimeField(auto_now_add=True)
    description = models.CharField(max_length = 500)
    owner = models.ForeignKey(
        USER_MODEL,
        on_delete=models.CASCADE,
        null=True,
        related_name="meal_plans"
    )
    recipes = models.ManyToManyField("recipes.Recipe", related_name="meal_plans")