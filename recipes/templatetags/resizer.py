from django import template

register = template.Library()

# def resize_to(ingredient, target):
#     print("value:", value)
#     print("arg:", arg)
#     return "resize done"


def resize_to(ingredient, target):
    number_of_servings = ingredient.recipe.servings

    if number_of_servings is not None and target is not None:
        try:
            ratio = int(target) / number_of_servings
            return ratio * ingredient.amount
        except ValueError:
            pass
    return ingredient.amount

register.filter(resize_to)

