from lib2to3.pgen2.literals import test
from unittest import TestCase
from recipes.models import Ingredient, Recipe
from recipes.templatetags.resizer import resize_to
#from meal_plans.models import MealPlan
#from django.db import IntegrityError



class ResizeToTests(TestCase):
    def test_error_when_ingredient_is_none(self):
         with self.assertRaises(AttributeError):
            resize_to(None, 3)
        

    def test_recipe_has_no_serving(self):
        recipe = Recipe(servings=None)
        ingredient = Ingredient(recipe=recipe, amount=5)
        
        result = resize_to(ingredient, None)
        self.assertEqual(5, result)


    def test_resize_to_is_none(self):
        recipe = Recipe(servings=2)
        ingredient = Ingredient(recipe=recipe, amount=5)

        result = resize_to(ingredient, None)
        self.assertEqual(5, result)


    def test_values_for_servings_amount_and_target(self):
        recipe = Recipe(servings=2)
        ingredient = Ingredient(recipe=recipe, amount=5)

        result = resize_to(ingredient, 10)
        self.assertEqual(25, result)

    def test_target_is_letters(self):
        recipe = Recipe(servings=2)
        ingredient = Ingredient(recipe=recipe, amount=5)

        result = resize_to(ingredient, "abc")
        self.assertEqual(5, result)
    


# class FoodItemTestCorrectLength(TestCase):
#     def test_name_correct_length(self):
#         FoodItem.objects.create(name="flour")

#         test_food_item = FoodItem.objects.get(name="flour")
#         max_characters = 100
#         total_characters = len(test_food_item.name)
#         self.assertLessEqual(total_characters, max_characters)
#         FoodItem.objects.filter(name="flour").delete()

#     def test_uniqueness(self):
#         pass
