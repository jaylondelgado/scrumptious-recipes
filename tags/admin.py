from django.contrib import admin
from tags.models import Tag 

class TagAdmin(admin.ModelAdmin):
    pass
# Register your models here.

admin.site.register(Tag, TagAdmin)