from django.urls import path
from django.contrib.auth import views as auth_views
from tags.views import (
    TagCreateView,
    TagUpdateView,
    TagDeleteView,
    TagDetailView,
    TagListView,
)
from tags.views import TagListView

urlpatterns = [
    path("", TagListView.as_view(), name="tags_list"),
    path("<int:pk>/", TagDetailView.as_view(), name="tag_detail"),
    path("<int:pk>/delete/", TagDeleteView.as_view(), name="tag_delete"),
    path("new/", TagCreateView.as_view(), name="tag_new"),
    path("<int:pk>/edit/", TagUpdateView.as_view(), name="tag_edit"),
    path("accounts/login/", auth_views.LoginView.as_view(), name="login"),
    path("accounts/logout/", auth_views.LogoutView.as_view(), name="logout"),
]
