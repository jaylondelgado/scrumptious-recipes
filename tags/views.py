from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from tags.models import Tag
from recipes.models import Recipe


# Create your views here.
class TagListView(ListView):
    model = Tag
    template_name = "tags/list.html"


class TagDetailView(LoginRequiredMixin, DetailView):
    model = Tag
    template_name = "tags/detail.html"

class TagCreateView(LoginRequiredMixin, CreateView):
    model = Tag
    template_name = "tags/new.html"

class TagDeleteView(LoginRequiredMixin, DeleteView):
    model = Tag
    template_name = "tags/delete.html"

class TagUpdateView(LoginRequiredMixin, UpdateView):
    model = Tag
    template_name = "tags/new.html"